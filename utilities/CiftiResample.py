import subprocess

cifti_in="~/data/Aktiva/Data/derivatives/sub-01/anat/sub-01_hemi-R_pial.surf.gii"
#direction="COLUMN"
#cifti_template=""
#template_direction="COLUMN"
surface_method="BARYCENTRIC"
#vol_method="CUBIC"
cifti_out="~/data/Aktiva/Data/derivatives/sub-01/anat/sub-01_hemi-R_pial_fsLR_32k.surf.gii"

#left_opts="-left-spheres ~/data/Aktiva/Data/derivatives/freesurfer/sub-01/surf/lh.sphere ~/utils/HCP/HCPpipelines/global/templates/standard_mesh_atlases/L.sphere.32k_fs_LR.surf.gii"
#right_opts="-right-spheres ~/data/Aktiva/Data/derivatives/freesurfer/sub-01/surf/rh.sphere ~/utils/HCP/HCPpipelines/global/templates/standard_mesh_atlases/R.sphere.32k_fs_LR.surf.gii"

cursphere="~/data/Aktiva/Data/derivatives/conversions/rh.sphere.reg.surf.gii"
newsphere="~/utils/HCP/HCPpipelines/global/templates/standard_mesh_atlases/R.sphere.32k_fs_LR.surf.gii"

final_call="wb_command -surface-resample {insurf} {cursphere} {newsphere} {method} {outsurf}".format(
    insurf=cifti_in,
    cursphere=cursphere,
    newsphere=newsphere,
    method=surface_method,
    outsurf=cifti_out
)
print(final_call)

subprocess.call(final_call,shell=True)