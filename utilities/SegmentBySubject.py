# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 13:48:13 2022

@author: Joseph Humphries
"""

import os
import shutil
import pydicom as dcm

#define data location
dcm_dir = '/home/humphries/data/FIRMM_test_data/DTI/nda_abcd'
dcm_files = os.listdir(dcm_dir)

for x in dcm_files:
    #load in dicom, extract series number
    if os.path.isfile(x):
        fpath = os.path.join(dcm_dir,x)
        dcmfile =  dcm.dcmread(fpath)
        ptID = str(dcmfile['PatientID'].value)
        dcmfile.save_as(fpath)
    
        ptdir = os.path.join(dcm_dir,ptID)
        
        if not os.path.isdir(ptdir):
            os.mkdir(ptdir)
        
        shutil.move(x, os.path.join(ptdir,x))
