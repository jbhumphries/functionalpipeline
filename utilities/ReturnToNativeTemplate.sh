#!/bin/bash

# this script has no inputs but filenames you need to accommodate to your use and working environment
# first need to convert ciftis to metric (.gii) file

## Some definitions
export path_to_wb=/usr/bin/wb_command
## In the following lines 
## You can replace $path_to_wb for wb_command if you use an installed version of workbench (module load workbench/1.4.... in MSI)

#export dscalar_file="zClusters.dscalar.nii"  # the cifti file you want to convert
#export path_to_files="/home/humphries/data/Aktiva/Data/Resampled"  # path  to OUTPUT FOLDER (is where the *.gii are going to be stored)

############ don't change #################
export left_cortex_metric=left_metric.shape.gii 
export right_cortex_metric=right_metric.shape.gii
########################################

###### Convert dscalar file ( Communities cifti file) to Metric file   #######
## LEFT CORTEX
$path_to_wb -cifti-separate $path_to_files/$dscalar_file \
COLUMN -metric CORTEX_LEFT $path_to_files/$left_cortex_metric
## RIGHT CORTEX
$path_to_wb -cifti-separate $path_to_files/$dscalar_file \
COLUMN -metric CORTEX_RIGHT $path_to_files/$right_cortex_metric
###########################################################

## More definitions    : CHANGE CAPITAL WORDS ACCORDING TO YOUR EXISTING FOLDER STRUCTURE #########################
#export left_surface=/home/humphries/data/Aktiva/Data/derivatives/sub-01/anat/sub-01_hemi-L_pial_fsLR_32k.surf.gii
#export right_surface=/home/humphries/data/Aktiva/Data/derivatives/sub-01/anat/sub-01_hemi-R_pial_fsLR_32k.surf.gii
export output_left=$path_to_files/left_vol_out.nii
export output_right=$path_to_files/right_vol_out.nii
#export reference_in_mni=/home/humphries/data/Aktiva/Data/derivatives/sub-01/anat/sub-01_desc-preproc_T1w.nii.gz
export nearest_vertex=2 # in mm

############### CONVERT METRIC FILE INTO VOLUME  #########################
## LEFT CORTEX
$path_to_wb -metric-to-volume-mapping $path_to_files/$left_cortex_metric $left_surface \
$reference_in_mni $output_left -nearest-vertex 2
## RIGHT CORTEX
$path_to_wb -metric-to-volume-mapping $path_to_files/$right_cortex_metric $right_surface \
$reference_in_mni $output_right -nearest-vertex $nearest_vertex
###########################################################################

# the last step is to add left and right volumes into one
# this can be done with fslmaths

#module load fsl ## Or your installed version of FSL
export FSLOUTPUTTYPE="NIFTI_GZ"
#/usr/local/fsl//bin/fslmaths $output_left -add $output_right OUTPUT_VOLUME

####################################################################### END #######################################################
