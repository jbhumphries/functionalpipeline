# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 15:04:33 2021

@author: Joseph Humphries
"""

#import packages 
import os
import shutil
from pathlib import Path
import pydicom as dcm

#define data location
sub_dir = '/home/humphries/data/ABCD_DTI'

#define subject ID
sub_id = 'sub-NDARINV2ZLBBMTU'

#extract list of files in flat dicom folder
flat_dir = Path(os.path.join(sub_dir,sub_id,'dcm'))
flat_files = [x for x in flat_dir.iterdir() if x.is_file()]

#loop through files
for x in flat_files:
    #load in dicom, extract series number
    dcmfile =  dcm.dcmread(x)
    SN = str(dcmfile['SeriesNumber'].value)
    
    #define series directory
    series_dir = os.path.join(flat_dir,SN)
    
    #make new directory if needed
    if not os.path.isdir(series_dir):
        os.mkdir(series_dir)
    
    shutil.move(x, os.path.join(sub_dir,sub_id,'dcm',SN,x.name))
