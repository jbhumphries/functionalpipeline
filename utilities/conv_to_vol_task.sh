#!/bin/bash

## Some definitions
export path_to_wb=/usr/bin/wb_command
export sub_anat=/home/humphries/data/AktivaUMN/Data/derivatives/conversions
#export path_to_wb=/panfs/roc/msisoft/workbench/1.4.2/bin/wb_command
#export dscalar_file=out_zmap_LmRhand.dscalar.nii
#export dscalar_file=out_DT_SERIES_resampled_all_frames_at_FD_0.2_ROI18.dscalar.nii
#full path
dscalar_file=${1}
greedy=${2}

echo "Greedy is : ""$greedy"""

if [ "$greedy" = TRUE ] ; then
      echo "Greedy mode indicated. Partial volumes will not be used. Good for categorical data."
else
      echo "Greedy var is empty. Data will be antialiased."
      greedy= 
fi

dscalar_base=$(basename "$dscalar_file")
echo ${dscalar_base}

#remove .dscalar.nii
dscalar_short_base=${dscalar_base::-12} 

path_to_files=$(dirname "$dscalar_file")
#export path_to_files=$abs_dir/Desktop/infomap/seed_map/MSC_example_motor_task_seed/msc01_half1
#export path_to_files=/home/miran045/moral453/Desktop/code/cifti_vol

export left_cortex_metric=left_metric.shape.gii
export right_cortex_metric=right_metric.shape.gii
#export subc_metric=subcortical.nii

$path_to_wb -cifti-separate $path_to_files/"$dscalar_base" \
COLUMN -metric CORTEX_LEFT $path_to_files/$left_cortex_metric

$path_to_wb -cifti-separate $path_to_files/"$dscalar_base" \
COLUMN -metric CORTEX_RIGHT $path_to_files/$right_cortex_metric

#$path_to_wb -cifti-separate $path_to_files/"$dscalar_base" \
#COLUMN -volume-all $path_to_files/$subc_metric

## More definitions

#export input=$path_to_files/$metric_file
export left_pial_surface=$sub_anat/sub-01_hemi-L_pial_fsLR_32k.surf.gii
export right_pial_surface=$sub_anat/sub-01_hemi-R_pial_fsLR_32k.surf.gii
export left_midthickness_surface=$sub_anat/sub-01_hemi-L_midthickness_fsLR_32k.surf.gii
export right_midthickness_surface=$sub_anat/sub-01_hemi-R_midthickness_fsLR_32k.surf.gii
export left_white_surface=$sub_anat/lh.smoothwm.fsLR.32k.surf.gii
export right_white_surface=$sub_anat/rh.smoothwm.fsLR.32k.surf.gii
export output_left=$path_to_files/${dscalar_short_base}_left_vol_out.nii
export output_right=$path_to_files/${dscalar_short_base}_right_vol_out.nii
export output_left_ribbon=$path_to_files/${dscalar_short_base}_left_ribbon_vol_out.nii
export output_right_ribbon=$path_to_files/${dscalar_short_base}_right_ribbon_vol_out.nii
#export reference_in_mni=$abs/miran045/shared/data/MSC_to_DCAN/sub-MSC01/ses-func01/files/MNINonLinear/T1w_restore.nii.gz
#export path_to_xfms=$abs/miran045/shared/data/MSC_to_DCAN/sub-MSC01/ses-func01/files/MNINonLinear/xfms
#export non_linear_warp=$abs/miran045/moral453/Desktop/infomap/seed_map/MSC_example2_split_half/standard2acpc_dc.nii.gz  #standard2acpc_dc.nii.gz
#export non_linear_warp=$path_to_xfms/standard2task-motor01.nii.gz   #standard2acpc_dc.nii.gz
#export reference=$abs/miran045/shared/data/MSC_to_DCAN/sub-MSC01/ses-func01/files/T1w/T1w_acpc_dc_restore.nii.gz
export reference_in_mni=$sub_anat/sub-01_desc-preproc_T1w.nii.gz
export nearest_vertex=2 # mm

#nerest verttex method
#$path_to_wb -metric-to-volume-mapping $path_to_files/$left_cortex_metric $left_midthickness_surface \
#$reference_in_mni $output_left -nearest-vertex 2 # 2 mm

#$path_to_wb -metric-to-volume-mapping $path_to_files/$right_cortex_metric $right_midthickness_surface \
#$reference_in_mni $output_right -nearest-vertex $nearest_vertex # 2 mm

echo "trying ribbon constrained metric to volume"
#ribbon method
if [ "$greedy" = TRUE ] ; then
$path_to_wb -metric-to-volume-mapping $path_to_files/$left_cortex_metric $left_midthickness_surface $reference_in_mni  "$output_left_ribbon" -ribbon-constrained $left_white_surface  $left_pial_surface -greedy
$path_to_wb -metric-to-volume-mapping $path_to_files/$right_cortex_metric $right_midthickness_surface $reference_in_mni  "$output_right_ribbon" -ribbon-constrained $right_white_surface  $right_pial_surface -greedy
else
$path_to_wb -metric-to-volume-mapping $path_to_files/$left_cortex_metric $left_midthickness_surface $reference_in_mni  "$output_left_ribbon" -ribbon-constrained $left_white_surface  $left_pial_surface 
$path_to_wb -metric-to-volume-mapping $path_to_files/$right_cortex_metric $right_midthickness_surface $reference_in_mni  "$output_right_ribbon" -ribbon-constrained $right_white_surface  $right_pial_surface 
fi

### Convert Volume from Standard to Subject space
#module load fsl

#echo "unwarping to native."
#re=$(echo "$output_left" | grep -b -o ".nii" | awk 'BEGIN {FS=":"}{print $1}')
#export out_left_warped=${output_left:0:$re}_warped.nii

#re=$(echo "$output_right" | grep -b -o ".nii" | awk 'BEGIN {FS=":"}{print $1}')
#export out_right_warped=${output_right:0:$re}_warped.nii

#re=$(echo "$output_left_ribbon" | grep -b -o ".nii" | awk 'BEGIN {FS=":"}{print $1}')
#export out_left_ribbon_warped=${output_left_ribbon:0:$re}_warped.nii

#re=$(echo "$output_right_ribbon" | grep -b -o ".nii" | awk 'BEGIN {FS=":"}{print $1}')
#export out_right_ribbon_warped=${output_right_ribbon:0:$re}_warped.nii

#re=$(echo "$path_to_files"/"$subc_metric" | grep -b -o ".nii" | awk 'BEGIN {FS=":"}{print $1}')
#export out_subc_warped=${subc_metric:0:$re}_warped.nii

#applywarp -i $output_left -o "$out_left_warped" -r $reference \
#-w $non_linear_warp --interp=nn
#applywarp -i $output_right -o "$out_right_warped" -r $reference \
#-w $non_linear_warp --interp=nn

#applywarp -i $output_left_ribbon -o "$out_left_ribbon_warped" -r $reference \
#-w $non_linear_warp --interp=nn
#applywarp -i $output_right_ribbon -o "$out_right_ribbon_warped" -r $reference \
#-w $non_linear_warp --interp=nn

#applywarp -i  $path_to_files/$subc_metric -o $path_to_files/${dscalar_short_base}_$out_subc_warped -r $reference \
#-w $non_linear_warp --interp=nn

#combine files back togather:
#fslmaths "$out_left_warped" -add "$out_right_warped" -add $path_to_files/${dscalar_short_base}_$out_subc_warped $path_to_files/${dscalar_short_base}_nativewarped_combined
export FSLOUTPUTTYPE="NIFTI_GZ"
/usr/local/fsl/bin/fslmaths "$output_left_ribbon" -add "$output_right_ribbon" $path_to_files/${dscalar_short_base}_ribbons_combined

#cp $reference $path_to_files/

#module load afni

#fslmaths $out_left_warped -thr 10 -recip -mul 10 -thr 1 $vol_roi_out

#export vol_roi_out=$path_to_files/vol_roi.nii.gz

#3dmaskdump -mask $vol_roi_out -noijk -xyz -o coords_vol_mask.txt $vol_roi_out #These coordinates are in the 'RAI' (DICOM) order.

