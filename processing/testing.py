from master_pipe import resample_surfs
import subprocess
import os

deriv_dir="/home/humphries/data/Aktiva/Data/derivatives"
fs_dir=os.path.join(deriv_dir,"freesurfer")

resample_surfs(fs_dir,deriv_dir)