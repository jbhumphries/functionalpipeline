import string
import subprocess, glob, os, argparse
import sys
import numpy as np
from pathlib import Path
from numpy.random import normal
import nibabel as nb
import pandas as pd
from nilearn.glm.first_level import make_first_level_design_matrix
from nilearn.glm.first_level import run_glm
from nilearn.glm.contrasts import compute_contrast
import pydicom as dcm

def get_cli_args():
    parser = argparse.ArgumentParser()

def create_dirs(basedir,dcmdir,bids_dir,deriv_dir,rest_xcp_out,task_results):
    print("Creating Directories...")
    subprocess.call("mkdir -p {0}".format(basedir))
    subprocess.call("mkdir -p {0}".format(dcmdir))
    subprocess.call("mkdir -p {0}".format(bids_dir))
    subprocess.call("mkdir -p {0}".format(deriv_dir))
    subprocess.call("mkdir -p {0}".format(rest_xcp_out))
    subprocess.call("mkdir -p {0}".format(os.path.join(task_results,'SubjectSpace')))

def get_TR(BOLD_DICOM):
    #input absolute filename path
    dcmfile = dcm.dcmread(BOLD_DICOM)
    TR = dcmfile["RepetitionTime"].value
    TR = TR/1000 #convert from ms to seconds
    return TR

def is_ME(BOLD_DICOM_FOLDER):
    dcm_files = glob.glob(BOLD_DICOM_FOLDER)
    for echo in [0,1]:
        if echo==1:
            tempdcm = dcm.dcmread(dcm_files[echo])
            e1 = tempdcm["EchoTime"].value
        else:
            tempdcm = dcm.dcmread(dcm_files[echo])
            e2 = tempdcm["EchoTime"].value
    
    if e1==e2:
        return False
    else: return True

def run_dcm2bids(dcm_dir,bids_dir,config):
    bids_call="dcm2bids -d {dcm} -c {conf} -o {bids} -p 01".format(dcm=dcm_dir,conf=config,bids=bids_dir)
    subprocess.call(bids_call,shell=True)

def run_fmriprep(bids_dir,fmriprep_dir):
    fs_license_dir='/home/humphries/data/Aktiva/Data'
    container_name='nipreps/fmriprep:latest'

    #options
    computer_options='--nthreads 8' #adjust based on computing environment
    subject_options='--participant-label 01 --cifti'
    output_options='--cifti-output 91k --output-layout bids'
    space_options='--output-spaces MNI152NLin2009cAsym T1w fsnative'
    license_options='--fs-license-file /license/license.txt'

    #add logic to use synthetic fieldmaps if none exist

    #write docker call and run
    docker_call="docker run --rm -it -v {indir}:/in/ -v {outdir}:/out/ -v {licensedir}:/license/ {container}".format(
        indir=bids_dir,
        outdir=fmriprep_dir,
        licensedir=fs_license_dir,
        container=container_name
    )

    fmriprep_call="{comp} {subj} {out} {space} {lic}".format(
        comp=computer_options,
        subj=subject_options,
        out=output_options,
        space=space_options,
        lic=license_options
    )

    final_call="{dock} {fprep} /in/ /out/ participant".format(dock=docker_call,fprep=fmriprep_call)
    print(final_call)

    subprocess.call(final_call,shell=True)

def run_rest_xcp(indir, outdir):
    #set file paths
    container_name='pennlinc/xcp_d:latest'

    #options
    computer_options='--nthreads 1'
    subject_options='--participant_label 01 -t rest --cifti --input-type fmirprep'
    postproc_options='--smoothing 6 --despike -p 36P'
    tempfilt_options='--lower-bpf 0.009 --upper-bpf 0.08'
    respfilt_options='--motion-filter-type notch --band-stop-min 12 --band-stop-max 18'
    scrub_options='-r 50 -f 0.5 -d 4'

    #write docker call and run
    docker_call="docker run --rm -it -v {indir}:/in/ -v {outdir}:/out/ {container}".format(
        indir=indir,
        outdir=outdir,
        container=container_name
    )

    xcp_call="{comp} {subj} {postproc} {temp} {resp} {scrub}".format(
        comp=computer_options,
        subj=subject_options,
        postproc=postproc_options,
        temp=tempfilt_options,
        resp=respfilt_options,
        scrub=scrub_options
    )

    final_call="{dock} /in/ /out/ {xcp}".format(dock=docker_call,xcp=xcp_call)
    print(final_call)

    subprocess.call(final_call,shell=True)

def extract_cifti_data(fname):
    cifti = nb.load(fname)
    cifti_data = cifti.get_fdata(dtype=np.float32)
    cifti_hdr = cifti.header
    nifti_hdr = cifti.nifti_header
    return cifti, cifti_data, cifti_hdr, nifti_hdr

def check_zeros_and_interp(restdir):
    import os
    import nibabel as nb
    import numpy as np
    import nibabel.testing
    from numpy.random import normal

    cii_fname = 'sub-01_task-rest_space-fsLR_den-91k_desc-residual_smooth_bold.dtseries.nii'
    cii_img, cii_data, cii_hdr, nii_hdr = extract_cifti_data(os.path.join(restdir, 'xcp-out/sub-01/func', cii_fname))

    round_data = np.round(cii_data,8)

    empty_vert = 0

    stdvec = np.std(round_data,axis=1)
    num_frames = len(stdvec)

    for i in range(0,91282):
        if np.all(round_data[:,i]==0,axis=0)==True:
            empty_vert+=1
    print('{verts} empty vertices detected in resting state. Check Freesurfer alignment. Interpolating values...'.format(verts=empty_vert))

    for i in range(0,91282):
        if np.all(round_data[:,i]==0,axis=0)==True:
            for j in range(0,num_frames):
                interp_mean = round_data[j,i-1]
                round_data[j,i] = normal(interp_mean,stdvec[j]) 
                
    matrix_data=round_data
    outfile=os.path.join(basedir,rawdir,'sub-01_task-rest_space-fsLR_den-91k_desc-residual_smooth_interp_bold.dtseries.nii')
    nii_matrix = nb.cifti2.load(os.path.join(restdir, 'xcp_d/sub-01/func', cii_fname))
    nb.cifti2.save(nb.cifti2.cifti2.Cifti2Image(
        dataobj = matrix_data,
        header = nii_matrix.header,
        nifti_header = nii_matrix.nifti_header,
        file_map = nii_matrix.file_map
        ), outfile)

def save_dscalar(matrix,outfile):
    example_file = "../utilities/example_dscalar.nii"
    nii_matrix = nb.cifti2.load(example_file)
    nb.cifti2.save(nb.cifti2.cifti2.Cifti2Image(
        dataobj = matrix,
        header = nii_matrix.header,
        nifti_header = nii_matrix.nifti_header,
        file_map = nii_matrix.file_map
        ), outfile)

def run_level1_task_analysis(infile,confounds_file,TR,event_file):

    #Run smoothing using workbench
    smoothed_infile="{infile}_smooth.dtseries.nii".format(infile=Path(Path(infile).stem).stem)
    smooth_cmd = "wb_command -cifti-smoothing {infile} 8 8 COLUMN {smooth} -fwhm -fix-zeros-volume -fix-zeros-surface".format(infile=infile,smooth=smoothed_infile)
    subprocess.call(smooth_cmd,shell=True)

    cifti, cifti_data, cifti_hdr, nifti_hdr = extract_cifti_data(smoothed_infile)

    events = pd.read_csv(event_file, sep="\t")
    #TODO: update file paths
    confounds = pd.read_csv(confounds_file,sep='\t')
    selected_confounds = confounds[['trans_x','trans_x_derivative1',
                                'trans_y','trans_y_derivative1','trans_z',
                                'trans_z_derivative1','rot_x','rot_x_derivative1',
                                'rot_y','rot_y_derivative1','rot_z',
                                'rot_z_derivative1']].copy()

    #change derivatives at frame 1 from n/a to 0
    selected_confounds = selected_confounds.replace(np.nan,0)

    
    n_frames = cifti_data.shape[0]
    frame_times = TR * (np.arange(n_frames))

    selected_confounds['frame_times']=frame_times
    selected_confounds.set_index('frame_times',drop=True,inplace=True)  

    design_matrix = make_first_level_design_matrix(frame_times,
                                                events=events,
                                                hrf_model='spm'
                                                )
    
    design_matrix = pd.concat([design_matrix,selected_confounds],axis=1)

    labels, estimates = run_glm(cifti_data, design_matrix.values.astype(int))

    #define contrasts
    #TODO: switch/case based on task ID
    emptycon=np.zeros((1,np.shape(design_matrix)[1])).flatten()

    leftcon=emptycon.copy()
    rightcon=emptycon.copy()

    leftcon[0] += 1
    rightcon[1] += 1

    conditions = {'Left': leftcon,
                  'Right': rightcon}
    contrast_spec= conditions['Left'] - conditions['Right']

    #compute z values
    contrast = compute_contrast(labels,estimates,contrast_spec,contrast_type='t')
    z_map = contrast.z_score()
    z_map[np.abs(z_map) < 2.75] = 0
    z_map = np.abs(z_map.reshape(1,91282))

    #output result cifti
    return z_map

def task_id_looper(fmriprep_dir):
    dtseries_list = glob.glob(os.path.join(fmriprep_dir,"*task*bold.dtseries.nii"))
    num_runs = len(dtseries_list)
    task_id_list = []

    for run in range(num_runs):
        fparts = dtseries_list[run].split(sep="task-")
        fparts = fparts[1]
        fparts = fparts.split(sep="_")
        task_id_list.append(fparts[0])
    
    unique_task_IDs = np.unique(task_id_list)
    return unique_task_IDs

def task_analysis_wrapper(fmriprep_dir,task_results,TR,site_data):

    task_IDs = task_id_looper(fmriprep_dir)

    for task_id in task_IDs:
        dtseries_list = glob.glob(os.path.join(fmriprep_dir,"*task-{id}*bold.dtseries.nii".format(id=task_id)))
        outfile=os.path.join(task_results,task_id,"{id}_zmap.dscalar.nii".format(id=task_id))
        subprocess.call("mkdir -p {0}".format(os.path.join(task_results,task_id)))

        if len(dtseries_list)>1:
            #get zmaps as outputs, average results, save average
            run_i=1
            events=os.path.join(site_data,"{id}_events.tsv".format(id=task_id))
            z_list = np.zeros(shape=(len(dtseries_list),91282))

            for run in dtseries_list:
                infile=run
                confounds=os.path.join(fmriprep_dir,"sub-01_task-{id}_run-{run_i}_desc-confounds_timeseries.tsv".format(id=task_id,run_i=run_i))
                
                temp_zmap = run_level1_task_analysis(infile,confounds,TR,events)
                z_list[run_i-1,:] = temp_zmap

            z_mean = np.mean(z_list,axis=0)
            save_dscalar(z_mean,outfile)

        elif len(dtseries_list)==1:
            infile=dtseries_list[0]
            confounds=os.path.join(fmriprep_dir,"sub-01_task-{id}_desc-confounds_timeseries.tsv".format(id=task_id))
            events=os.path.join(site_data,"{id}_events.tsv".format(id=task_id))

            z_map = run_level1_task_analysis(infile,confounds,TR,events)
            save_dscalar(z_map,outfile)
        else:
            raise RuntimeError("No task runs found in fmriprep outputs with task ID {id}".format(id=task_id))

def write_rest_conc(rest_xcp_out,dconn_dir):
    dtseries_list = glob.glob(os.path.join(rest_xcp_out,"*task-rest*bold.dtseries.nii"))
    dconn_fname = "{dconn_dir}/rest_dtseries.conf".format(dconn_dir=dconn_dir)
    subprocess.call("touch {dconn_fname}".format(dconn_fname=dconn_fname),shell=True)

    dconn = open("{dconn_fname}".format(dconn_fname=dconn_fname))
    for run in dtseries_list:
        dconn.write("{rest_run}\n".format(rest_run=run))
    
    dconn.close()

def cifti_conn(wb_path,mre_dir,TR,dconn_dir):
    dconn_call="python3 cifti_conn_wrapper.py --wb-command {wb_path} --mre-dir {mre_dir} {TR} {dconn_dir} matrix".format(
        wb_path=wb_path,
        mre_dir=mre_dir,
        conc_path="{dconn_dir}/rest_dtseries.conf".format(dconn_dir=dconn_dir),
        TR=TR,
        dconn_dir=dconn_dir
    )
    
    #wb_command probably at /usr/bin/wb_command
    #MRE installed at specific loc: default to ~/utils/MRE

    subprocess.call(dconn_call,shell=True)

def convert_fs_to_gii(fs_dir,fmriprep_dir):

    #right
    infile=os.path.join(fs_dir,"sub-01/surf/rh.sphere.reg")
    outfile=os.path.join(fmriprep_dir,"sub-01/anat/rh.sphere.reg.surf.gii")
    subprocess.call("mris_convert {0} {1}".format(infile,outfile),shell=True)

    #left
    infile=os.path.join(fs_dir,"sub-01/surf/lh.sphere.reg")
    outfile=os.path.join(fmriprep_dir,"sub-01/anat/lh.sphere.reg.surf.gii")
    subprocess.call("mris_convert {0} {1}".format(infile,outfile),shell=True)

def resample_surfs(fs_dir, fmriprep_dir):
    surface_method="BARYCENTRIC"

    #right
    pial_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_pial.surf.gii")
    pial_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_pial_fsLR_32k.surf.gii")

    wm_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_smoothwm.surf.gii")
    wm_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_smoothwm_fsLR_32k.surf.gii")

    mid_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_midthickness.surf.gii")
    mid_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_midthickness_fsLR_32k.surf.gii")
    
    regsphere=os.path.join(fs_dir,"sub-01/surf/rh.sphere.reg")
    newsphere="../utilities/fs_LR-deformed_to-fsaverage.R.sphere.32k_fs_LR.surf.gii"
    cursphere=os.path.join(fmriprep_dir,"sub-01/anat/rh.sphere.reg.surf.gii")

    prep_call="wb_shortcuts -freesurfer-resample-prep {wm_in} {pial_in} {regsphere} {newsphere} {mid_in} {mid_out} {cursphere}".format(
        wm_in=wm_in,
        pial_in=pial_in,
        regsphere=regsphere,
        newsphere=newsphere,
        mid_in=mid_in,
        mid_out=mid_out,
        cursphere=cursphere
    )

    pial_call="wb_command -surface-resample {insurf} {cursphere} {newsphere} {method} {outsurf}".format(
        insurf=pial_in,
        cursphere=cursphere,
        newsphere=newsphere,
        method=surface_method,
        outsurf=pial_out
    )

    wm_call="wb_command -surface-resample {insurf} {cursphere} {newsphere} {method} {outsurf}".format(
        insurf=wm_in,
        cursphere=cursphere,
        newsphere=newsphere,
        method=surface_method,
        outsurf=wm_out
    )

    subprocess.call(prep_call,shell=True,executable='/bin/bash')
    subprocess.call(pial_call,shell=True,executable='/bin/bash')
    subprocess.call(wm_call,shell=True,executable='/bin/bash')

    #left
    pial_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_pial.surf.gii")
    pial_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_pial_fsLR_32k.surf.gii")

    wm_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_smoothwm.surf.gii")
    wm_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_smoothwm_fsLR_32k.surf.gii")

    mid_in=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_midthickness.surf.gii")
    mid_out=os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_midthickness_fsLR_32k.surf.gii")
    
    regsphere=os.path.join(fs_dir,"sub-01/surf/lh.sphere.reg")
    newsphere="../utilities/fs_LR-deformed_to-fsaverage.L.sphere.32k_fs_LR.surf.gii"
    cursphere=os.path.join(fmriprep_dir,"sub-01/anat/lh.sphere.reg.surf.gii")


    prep_call="wb_shortcuts -freesurfer-resample-prep {wm_in} {pial_in} {regsphere} {newsphere} {mid_in} {mid_out} {cursphere}".format(
        wm_in=wm_in,
        pial_in=pial_in,
        regsphere=regsphere,
        newsphere=newsphere,
        mid_in=mid_in,
        mid_out=mid_out,
        cursphere=cursphere
    )

    pial_call="wb_command -surface-resample {insurf} {cursphere} {newsphere} {method} {outsurf}".format(
        insurf=pial_in,
        cursphere=cursphere,
        newsphere=newsphere,
        method=surface_method,
        outsurf=pial_out
    )

    wm_call="wb_command -surface-resample {insurf} {cursphere} {newsphere} {method} {outsurf}".format(
        insurf=wm_in,
        cursphere=cursphere,
        newsphere=newsphere,
        method=surface_method,
        outsurf=wm_out
    )

    subprocess.call(prep_call,shell=True,executable='/bin/bash')
    subprocess.call(pial_call,shell=True,executable='/bin/bash')
    subprocess.call(wm_call,shell=True,executable='/bin/bash')

    #fix midthickness cifti structure (otherwise reads as INVALID type, but works fine (I think))
    #if needed could generate new midthickness file from resampled pial and wm surfs
    subprocess.call("wb_command -set-structure sub-01_hemi-R_midthickness_fsLR_32k.surf.gii CORTEX_RIGHT -surface-secondary-type MIDTHICKNESS",shell=True)
    subprocess.call("wb_command -set-structure sub-01_hemi-L_midthickness_fsLR_32k.surf.gii CORTEX_LEFT -surface-secondary-type MIDTHICKNESS",shell=True)

def resample_task_subspace(task_results, task_id):
    subprocess.call("export path_to_files=\"{outloc}\"".format(outloc=os.path.join(task_results,'SubjectSpace')))
    subprocess.call("export left_surface={pial}".format(os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-L_pial_fsLR_32k.surf.gii")))
    subprocess.call("export right_surface={pial}".format(os.path.join(fmriprep_dir,"sub-01/anat/sub-01_hemi-R_pial_fsLR_32k.surf.gii")))
    subprocess.call("export reference_in_mni={T1}".format(os.path.join(fmriprep_dir,"sub-01/anat/sub-01_desc-preproc_T1w.nii.gz")))

    #zmap
    subprocess.call("export dscalar_file=\"{task}_zmap.dscalar.nii\"".format(task=task_id))
    subprocess.call("bash ../utilities/ReturnToNativeTemplate.sh")
    subprocess.call("/usr/local/fsl/bin/fslmaths $output_left -add $output_right {outfile}".format(outfile="{task_id}_SubjectSpace_zmap.dscalar.nii"))

def resample_rest_subspace(rest_results):
    #placeholder
    a=1

def nii_to_dcm(nii_in,dcm_in,dcm_out):
    #dcm_in should be example file, e.g. one BOLD frame
    #dcm_out is directory
    #all inputs should be given as full absolute paths

    nii = nb.load(nii_in)
    nii_data = nii.get_fdata()
    nii_data= nii_data.astype('uint16')

    #Need to know if:
    #we save each slice as separate dcm file, or:
    #save as mosaic image (and therefore need to convert 3d to mosaic)

    template_dcm = dcm.dcmread(dcm_in)

    def convert_slice(nii_slice,template_dcm,dcm_out,index=0):
        template_dcm.Rows = nii_slice.shape[0]
        template_dcm.Columns = nii_slice.shape[1]
        template_dcm.PhotometricInterpretation = 'MONOCHROME2'
        template_dcm.SamplesPerPixel = 1
        template_dcm.BitsStored = 16
        template_dcm.BitsAllocated = 16
        template_dcm.HighBit = 15
        template_dcm.PixelRepresentation = 1
        template_dcm.PixelData = nii_slice.tobytes()

        template_dcm.save_as(os.path.join(dcm_out, f'slice{index}.dcm'))
    
    num_slices = nii_data.shape[2]
    for slice_ in range(num_slices):
        convert_slice(nii_data[:,:,slice_],template_dcm,dcm_out,slice_)

def cleanup(fmriprep_dir,fs_dir,dcm_dir,dconn_dir):
    #Update as pipeline decisions change
    subprocess.call("rm -rf {fmriprep_dir}".format(fmriprep_dir),shell=True)
    subprocess.call("rm -rf {fs_dir}".format(fs_dir),shell=True)
    subprocess.call("rm -rf {dcm_dir}".format(dcm_dir),shell=True)
    subprocess.call("rm -rf {dconn_dir}".format(dconn_dir),shell=True)

if __name__=="__main__":

    #define file paths
    basedir='/home/humphries/data/subID/' #TODO: handle subject id passed in via command line
    dcm_dir=os.path.join(basedir,'dcm')
    bids_dir=os.path.join(basedir,'bids')
    deriv_dir=os.path.join(bids_dir,'derivatives')
    fmriprep_dir=os.path.join(deriv_dir,'fmriprep')
    fs_dir=os.path.join(deriv_dir,'freesurfer')
    rest_xcp_out=os.path.join(deriv_dir,'rest_xcp')
    task_results=os.path.join(deriv_dir,'task_results')
    site_data='/home/humphries/ref/'

    #call functions to:
    #1: make directories if they dont exist (done)
    #2: extract TR (done; need additional logic to identify which file to pull TR from; or just get from site during setup)
    #2: run dcm2bids (done)
    #3: run fmriprep (done)
    #4: run task xcp_d (done; need logic to check if task was done)
    #5: run rest xcp_d (done; need logic to check if rest was done)
    #6: check for empty rest voxels and interpolate or flag as error (done; need logic for edge cases and voxel count)
    #7: generate rest dconns (done)
    #8: run rest template matching (need containerized script)
    #9: run task analysis (template done; need logic to determine what tasks to run and how many, how to input event timings)
    #10: convert ?h.sphere.reg to sphere.reg.surf.gii (done)
    #11: generate 32k subject pials (done)
    #12: resample task to subject space (done)
    #13: resample rest to subject space (done)
    #14: clean up intermediate files (e.g. parcellations, dconns) (done)

    #Problems:
    #1) Need to know how task paradigm info and which tasks are coming in
    #2) Step completion notification
    #3) interpolation logic issues (check hemisphere, flag problem if too many missing vertices)
    #4) config.json editing to run dcm2bids (series numbers for tasks? Or just order of tasks may suffice)
    #5) need function to identify BOLD dicom scan (if we know the SequenceName (or protocol name), can loop over dcms, check sequence name, and extract TR of first *epfid2d1_96 or whatever)
