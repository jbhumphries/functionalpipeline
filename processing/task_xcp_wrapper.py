import subprocess

#set file paths
fmriprep_dir='/home/humphries/data/Aktiva/Data/derivatives'
output_dir='/home/humphries/data/Aktiva/Data/xcp_reproc'
container_name='pennlinc/xcp_d:latest'

#options
computer_options='--nthreads 1'
subject_options='--participant_label 01 --cifti --input-type fmirprep'
postproc_options='--smoothing 6 --despike -p 24P'
tempfilt_options='--lower-bpf 0.009 --upper-bpf 0.08'
scrub_options='-r 50 -f 0.9'

#write docker call and run
docker_call="docker run --rm -it -v {indir}:/in/ -v {outdir}:/out/ {container}".format(
    indir=fmriprep_dir,
    outdir=output_dir,
    container=container_name
)

xcp_call="{comp} {subj} {postproc} {temp} {scrub}".format(
    comp=computer_options,
    subj=subject_options,
    postproc=postproc_options,
    temp=tempfilt_options,
    scrub=scrub_options
)

final_call="{dock} /in/ /out/ {xcp}".format(dock=docker_call,xcp=xcp_call)
print(final_call)

subprocess.call(final_call,shell=True)