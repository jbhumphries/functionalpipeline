def run_rest_xcp(indir, outdir):
    #set file paths
    container_name='pennlinc/xcp_d:latest'

    #options
    computer_options='--nthreads 1'
    subject_options='--participant_label 01 -t rest --cifti --input-type fmirprep'
    postproc_options='--smoothing 6 --despike -p 36P'
    tempfilt_options='--lower-bpf 0.009 --upper-bpf 0.08'
    respfilt_options='--motion-filter-type notch --band-stop-min 12 --band-stop-max 18'
    scrub_options='-r 50 -f 0.5 -d 4'

    #write docker call and run
    docker_call="docker run --rm -it -v {indir}:/in/ -v {outdir}:/out/ {container}".format(
        indir=indir,
        outdir=outdir,
        container=container_name
    )

    xcp_call="{comp} {subj} {postproc} {temp} {resp} {scrub}".format(
        comp=computer_options,
        subj=subject_options,
        postproc=postproc_options,
        temp=tempfilt_options,
        resp=respfilt_options,
        scrub=scrub_options
    )

    final_call="{dock} /in/ /out/ {xcp}".format(dock=docker_call,xcp=xcp_call)
    print(final_call)

    subprocess.call(final_call,shell=True)