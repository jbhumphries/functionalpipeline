import subprocess

#set file paths
bids_dir='/home/humphries/data/Aktiva/Data/'
output_dir='/home/humphries/data/Aktiva/Data/derivatives'
fs_license_dir='/home/humphries/data/Aktiva/Data'
container_name='nipreps/fmriprep:21.0.0rc1'

#options
computer_options='--nthreads 1' #use --nprocs 4 if on MSI node
subject_options='--participant_label 01 --cifti --input-type fmirprep'
output_options='--cifti-output 91k --output-layout bids'
space_options='--output-spaces fsLR:den-91k fsLR:den-32k MNI152NLin2009cAsym T1w fsnative'
license_options='--fs-license-file /license/license.txt'

#add logic to use synthetic fieldmaps if none exist

#write docker call and run
docker_call="docker run --rm -it -v {indir}:/in/ -v {outdir}:/out/ -v {licensedir}:/license/ {container}".format(
    indir=bids_dir,
    outdir=output_dir,
    licensedir=fs_license_dir,
    container=container_name
)

fmriprep_call="{comp} {subj} {out} {space} {lic}".format(
    comp=computer_options,
    subj=subject_options,
    out=output_options,
    space=space_options,
    lic=license_options
)

final_call="{dock} {fprep} /in/ /out/ participant".format(dock=docker_call,fprep=fmriprep_call)
print(final_call)

subprocess.call(final_call,shell=True)