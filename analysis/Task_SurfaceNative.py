# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 11:33:33 2021

@author: Joseph Humphries
"""

import os
import nilearn.plotting as nlp
from nilearn import surface as nls
import nibabel as nb
import numpy as np
import nibabel.testing
import pandas as pd
from nilearn.image import load_img
import nibabel.cifti2.cifti2 as ci
import nibabel.cifti2.cifti2_axes as ci_ax

#basedir = '/home/humphries/data/Aktiva/Data'
#basedir = '\\\\wsl$\\Ubuntu-18.04\\home\\humphries\\data\\Aktiva\\Data'
basedir = 'C:/Users/Joseph Humphries/Documents/Data/Aktiva'
infdir = 'C:/Users/Joseph Humphries/Documents/Data/'
datadir = 'xcp/func'
cii_fname = 'sub-01_task-motorhand_space-fsLR_den-91k_desc-residual_smooth_den-91k_bold.dtseries.nii'

def extract_cifti_data(fname):
    cifti = nb.load(fname)
    cifti_data = cifti.get_fdata(dtype=np.float32)
    cifti_hdr = cifti.header
    nifti_hdr = cifti.nifti_header
    return cifti, cifti_data, cifti_hdr, nifti_hdr

cifti, cifti_data, cifti_hdr, nifti_hdr = extract_cifti_data(os.path.join(basedir, datadir, cii_fname))

axes = [cifti_hdr.get_axis(i) for i in range(cifti.ndim)]

#extract data in volume
def volume_from_cifti(data, axis):
    assert isinstance(axis, nb.cifti2.BrainModelAxis)
    data = data.T[axis.volume_mask]                          # Assume brainmodels axis is last, move it to front
    volmask = axis.volume_mask                               # Which indices on this axis are for voxels?
    vox_indices = tuple(axis.voxel[volmask].T)      # ([x0, x1, ...], [y0, ...], [z0, ...])
    vol_data = np.zeros(axis.volume_shape + data.shape[1:],  # Volume + any extra dimensions
                        dtype=data.dtype)
    vol_data[vox_indices] = data                             # "Fancy indexing"
    return nb.Nifti1Image(vol_data, axis.affine)             # Add affine for spatial interpretation

def surf_data_from_cifti(data, axis, surf_name):
    assert isinstance(axis, nb.cifti2.BrainModelAxis)
    for name, data_indices, model in axis.iter_structures():  # Iterates over volumetric and surface structures
        if name == surf_name:                                 # Just looking for a surface
            data = data.T[data_indices]                       # Assume brainmodels axis is last, move it to front
            vtx_indices = model.vertex                        # Generally 1-N, except medial wall vertices
            surf_data = np.zeros((vtx_indices.max() + 1,) + data.shape[1:], dtype=data.dtype)
            surf_data[vtx_indices] = data
            return surf_data
    raise ValueError(f"No structure named {surf_name}")

def decompose_cifti(img):
    data = img.get_fdata(dtype=np.float32)
    brain_models = img.header.get_axis(1)  # Assume we know this
    return (volume_from_cifti(data, brain_models),
            surf_data_from_cifti(data, brain_models, "CIFTI_STRUCTURE_CORTEX_LEFT"),
            surf_data_from_cifti(data, brain_models, "CIFTI_STRUCTURE_CORTEX_RIGHT"))

vol, left, right = decompose_cifti(cifti)


surfdir = 'derivatives/freesurfer/sub-01/surf'
anatdir = 'xcp/anat'
#inf_L = os.path.join(infdir,'Conte69.L.inflated.32k_fs_LR.surf.gii')
#inf_R = os.path.join(infdir,'Conte69.R.inflated.32k_fs_LR.surf.gii')

mid_L = nls.load_surf_mesh(os.path.join(basedir,anatdir,'sub-01_hemi-L_midthickness.surf.gii'))

#inf_L = os.path.join(basedir, surfdir, 'lh.inflated')
#inf_R = os.path.join(basedir, surfdir, 'rh.inflated')

sulc_L = os.path.join(basedir, surfdir, 'lh.sulc')
sulc_R = os.path.join(basedir, surfdir, 'rh.sulc')

#%% more data
TR=2

event_fname = 'C:/Users/Joseph Humphries/Documents/Data/Aktiva/task-events_norest.tsv'
events = pd.read_csv(event_fname, sep="\t")
"""
FD_fname='sub-01_task-motorhand_space-fsLR_desc-framewisedisplacement_den-91k_bold.tsv'
FD = pd.read_csv(os.path.join(basedir, datadir, FD_fname), sep="\t")
FD.index = FD.index + 1
FD.loc[0] = 1
FD.sort_index(inplace=True)

FD_censor = FD>=0.9
FD_censor['frames'] = np.arange(1,300,2)
scrub_df = FD_censor.set_index('frames')
"""

#%% glm
from nilearn.glm.first_level import make_first_level_design_matrix
n_frames = cifti_data.shape[0]
frame_times = TR * (np.arange(n_frames) + .5)

design_matrix = make_first_level_design_matrix(frame_times,
                                               events=events,
                                               hrf_model='spm'
                                               )
#design_matrix['scrub'] = scrub_df

from nilearn.glm.first_level import run_glm
labels, estimates = run_glm(cifti_data, design_matrix.values.astype(int))

#%% contrasts
conditions = {'Left': np.array([1, 0, 0, 0, 0, 0, 0, 0, 0]),
              'Right': np.array([0, 1, 0, 0, 0, 0, 0, 0, 0])
              }


side_diff = conditions['Left'] - conditions['Right']
move = conditions['Left'] + conditions['Right']

#%% compute contrast stats

selected_contrast = move
con_name = 'side_diff'

from nilearn.glm.contrasts import compute_contrast
contrast = compute_contrast(labels,estimates,selected_contrast,contrast_type='t')
z_map = contrast.z_score()
z_map[np.abs(z_map) < 3] = 0
z_map = z_map.reshape(1,91282)
#%% save cifti
#zmap_axes = ci_ax.BrainModelAxis.from_mask(z_map)
#z_hdr = ci.Cifti2Header()
#z_cii = nb.cifti2.cifti2.Cifti2Image(z_map, header=ci.Cifti2Header.from_axes(zmap_axes))
#z_cii.update_headers()
#nb.cifti2.cifti2.save(z_cii,'statmap.nii')

#%% plotting

#right_inflated = nls.load_surf_mesh(inf_R)
#left_inflated = nls.load_surf_mesh(inf_L)

#right_mid
left_mid = nls.load_surf_mesh(os.path.join(infdir,'Conte69_Atlas/Conte69.L.midthickness.32k_fs_LR.surf.gii'))
#sulc_L = load_img(os.path.join(infdir,'Conte69_Atlas/fs_LR.32k.LR.sulc.dscalar.nii'))

#nlp.plot_surf_stat_map(left_inflated, z_map, hemi='left',
 #                  views=['lateral','medial'],
  #                 title=con_name, colorbar=True,
   #                threshold=3)

clust_thresh = 25
a = 0.05

#%% save
"""
    Save a numpy array into a cifti2 .nii file by importing an arbitrary cifti2
    matrix and saving a copy of it with its data replaced by the data of the
    the new matrix
    :param matrix_data: numpy.ndarray with data to save into cifti2 file
    :param example_file: String, the path to a .nii file with the right format
    :param outfile: String, the path to the output .nii file to save
    :return: N/A
"""
#example_file = os.path.join(basedir, datadir, 'BOLD_rowSum.dscalar.nii')
example_file=os.path.join(basedir, datadir, cii_fname)
matrix_data=np.tile(z_map,(150,1))
outfile=os.path.join(basedir,'zmap.dtseries.nii')
nii_matrix = nb.cifti2.load(example_file)
nb.cifti2.save(nb.cifti2.cifti2.Cifti2Image(
    dataobj = matrix_data,
    header = nii_matrix.header,
    nifti_header = nii_matrix.nifti_header,
    file_map = nii_matrix.file_map
    ), outfile)