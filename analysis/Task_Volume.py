# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 09:49:57 2021

Generalized task fmri template for both surface and volume processing.

Assumes fmriprep preprocessing and xcp-abcd postprocessing.

@author: Joseph Humphries
"""
#%% load in packages
import os
import pandas as pd
import numpy as np
from nilearn.image import load_img, resample_to_img, math_img
from nilearn.plotting import plot_stat_map, show, plot_surf_stat_map
from nilearn import surface
from nilearn.glm.first_level import make_first_level_design_matrix, run_glm, FirstLevelModel
from nilearn.glm.contrasts import compute_contrast
from nilearn.glm import threshold_stats_img


#%% set directory locations
fsdir = 'C:/Users/Joseph Humphries/Documents/Data/Aktiva/derivatives/freesurfer/'
workdir = 'C:/Users/Joseph Humphries/Documents/Data/Aktiva/'
datadir = os.path.join(workdir, 'xcp/out/xcp_abcd')
anatdir = os.path.join(workdir, 'derivatives/fmriprep')

#%% set task/subject/scan information
sub_id = '01'
task_id = 'motorfoot'
volatlas_id = 'MNI152NLin2009cAsym'
TR=2

#%% Define filenames
fmri_fname = os.path.join(datadir, 'sub-{sub_id}'.format(sub_id=sub_id),
                             'func',
                             'sub-{sub_id}_task-{task_id}_space-{volatlas_id}_desc-residual_smooth_bold.nii.gz'.format(
                                 sub_id=sub_id,
                                 task_id=task_id,
                                 volatlas_id=volatlas_id))

events_fname = os.path.join(workdir, 'task-events_norest.tsv')
                            
confounds_fname = os.path.join(anatdir,'sub-{sub_id}'.format(sub_id=sub_id),
                                     'func',
                                     'sub-{sub_id}_task-{task_id}_desc-confounds_timeseries.tsv'.format(
                                         sub_id=sub_id,
                                         task_id=task_id))

FD_fname = os.path.join(datadir, 'sub-{sub_id}'.format(sub_id=sub_id),
                                 'func', 
                                 'sub-{sub_id}_task-{task_id}_space-{volatlas_id}_desc-framewisedisplacement_bold.tsv'.format(
                                     sub_id=sub_id,
                                     task_id=task_id,
                                     volatlas_id=volatlas_id))

anat_fname = os.path.join(anatdir, 'sub-{sub_id}'.format(sub_id=sub_id),
                                 'anat',
                                 'sub-{sub_id}_space-{volatlas_id}_desc-preproc_T1w.nii.gz'.format(
                                     sub_id=sub_id,
                                     volatlas_id=volatlas_id))

mean_fname = os.path.join(anatdir, 'sub-{sub_id}'.format(sub_id=sub_id),
                                  'func',
                                  'sub-{sub_id}_task-{task_id}_space-{volatlas_id}_boldref.nii.gz'.format(
                                      sub_id=sub_id,
                                      task_id=task_id,
                                      volatlas_id=volatlas_id))

GMmask_fname = os.path.join(anatdir, 'sub-{sub_id}'.format(sub_id=sub_id),
                                 'anat',
                                 'sub-{sub_id}_space-{volatlas_id}_label-GM_probseg.nii.gz'.format(
                                     sub_id=sub_id,
                                     volatlas_id=volatlas_id))

#%% Load images and CSVs
anat_img = load_img(anat_fname)

mean_fmri = load_img(mean_fname)

fmri_img = load_img(fmri_fname)

mask_img = load_img(GMmask_fname)

events = pd.read_csv(events_fname,sep="\t")

confounds = pd.read_csv(confounds_fname, sep="\t")

FD = pd.read_csv(FD_fname, sep="\t")

#%% Load surfaces
#nilearn complains if you aren't in the freesurfer directory
os.chdir(os.path.join(fsdir,'fsaverage','xhemi','surf'))

right_pial = surface.load_surf_mesh('./rh.pial')
left_pial = surface.load_surf_mesh('./lh.pial')

right_sulc = surface.load_surf_data('./rh.sulc')
left_sulc = surface.load_surf_data('./lh.sulc')

right_inflated = surface.load_surf_mesh('./rh.inflated')
left_inflated = surface.load_surf_mesh('./lh.inflated')

#%% Additional data prep
FD.loc[0] = 1
FD.sort_index(inplace=True)

FD_censor = FD>=0.5
FD_censor['frames'] = np.arange(1,300,2)
scrub_df = FD_censor.set_index('frames')

resampled_mask = resample_to_img(mask_img, fmri_img)
mask_img = math_img('img > 0.5', img=resampled_mask)

right_texture = surface.vol_to_surf(fmri_img, right_pial)
left_texture = surface.vol_to_surf(fmri_img, left_pial)

#%% Create GLM - surface processing
n_frames = right_texture.shape[1]
frame_times = TR * (np.arange(n_frames) + .5)

surf_matrix = make_first_level_design_matrix(frame_times,
                                               events=events,
                                               hrf_model='spm'
                                               )
surf_matrix['scrub'] = scrub_df
labels, estimates = run_glm(right_texture.T, surf_matrix.values.astype(int))

#%% Create GLM - volume processing
vol_glm = FirstLevelModel(t_r=TR,
                           noise_model='ar1',
                           standardize=False,
                           hrf_model='spm',
                           drift_model='cosine',
                           smoothing_fwhm=6)

vol_glm = vol_glm.fit(fmri_img, events)

vol_matrix = vol_glm.design_matrices_[0]

#%% Create contrasts 
#TODO: Generalize contrast generation if possible

#example: left and right movement
conditions = {'Left': np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
              'Right': np.array([0, 1, 0, 0, 0, 0, 0, 0, 0, 0])
             }

side_diff = conditions['Left'] - conditions['Right']

#useful for swapping out contrast names
selected_contrast = side_diff
con_name = 'side_diff'

#%% run and plot surface GLM 
contrast = compute_contrast(labels,estimates,selected_contrast,contrast_type='t')
z_map = contrast.z_score()

plot_surf_stat_map(right_inflated, z_map, hemi='right',
                   views=['lateral','medial'],
                   title=con_name, colorbar=True,
                   threshold=3., bg_map=right_sulc)

#%% run and plot volume GLM
f_map = vol_glm.compute_contrast(selected_contrast,
                                    output_type='effect_size')

z_map = vol_glm.compute_contrast(selected_contrast,
                                  output_type='z_score')

_, threshold = threshold_stats_img(z_map, alpha=.005, height_control='fpr')

clust_thresh = 25
a = 0.05

clean_map, threshold = threshold_stats_img(
    z_map, alpha=a, height_control='fdr', cluster_threshold=clust_thresh)

#volumetric plotting
plot_stat_map(clean_map, bg_img=anat_img, threshold=threshold,
              display_mode='y', cut_coords=6, black_bg=True,
              title='Obj Naming (fdr={a}), clusters > {clust_thresh} voxels'.format(a=a,clust_thresh=clust_thresh))
show()

plot_stat_map(clean_map, bg_img=anat_img, threshold=threshold,
              display_mode='x', cut_coords=6, black_bg=True,
              title='Obj Naming (fdr={a}), clusters > {clust_thresh} voxels'.format(a=a,clust_thresh=clust_thresh))
show()

plot_stat_map(clean_map, bg_img=anat_img, threshold=threshold,
              display_mode='z', cut_coords=6, black_bg=True,
              title='Obj Naming (fdr={a}), clusters > {clust_thresh} voxels'.format(a=a,clust_thresh=clust_thresh))
show()

#plot surface projections of volume results
right_texture = surface.vol_to_surf(clean_map, right_pial)
left_texture = surface.vol_to_surf(clean_map, left_pial)

plot_surf_stat_map(right_inflated, right_texture, hemi='right',
                            title='Surface right hemisphere', colorbar=True,
                            threshold=0.5, bg_map=right_sulc)

plot_surf_stat_map(left_inflated, left_texture, hemi='left',
                            title='Surface left hemisphere', colorbar=True,
                            threshold=0.5, bg_map=left_sulc)